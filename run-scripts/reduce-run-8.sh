#!/bin/bash
#SBATCH --account=icts
#SBATCH --partition=curie
#SBATCH --nodes=1 --ntasks=8
#SBATCH --time=60:00
#SBATCH --mem-per-cpu=4000
#SBATCH --job-name="OpenMPReduce3Performance"
#SBATCH --mail-user=RBBKYL001@myuct.ac.za
#SBATCH --mail-type=BEGIN,END,FAIL

cd ~/pi-calc/
make pi-reduce

export OMP_NUM_THREADS=2
echo "iteration,total_iterations,numcores,numthreads,time,pi"
for i in {1..20}; do
    ./pi-reduce $i $SLURM_TASKS_PER_NODE
done

export OMP_NUM_THREADS=4
for i in {1..20}; do
    ./pi-reduce $i $SLURM_TASKS_PER_NODE
done

export OMP_NUM_THREADS=8
for i in {1..20}; do
    ./pi-reduce $i $SLURM_TASKS_PER_NODE
done
