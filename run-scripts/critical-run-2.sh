#!/bin/bash
#SBATCH --account=icts
#SBATCH --partition=curie
#SBATCH --nodes=1 --ntasks=2
#SBATCH --time=60:00
#SBATCH --mem-per-cpu=4000
#SBATCH --job-name="OMPCriticalPerformance"
#SBATCH --mail-user=ABRSAS002@myuct.ac.za
#SBATCH --mail-type=BEGIN,END,FAIL

export OMP_NUM_THREADS=2

cd /home/hpc24/openMP_timing
for i in {1..20}; do
   ./pi_critical $i 2
done

export OMP_NUM_THREADS=4

for i in {1..20}; do
    ./pi_critical $i 2
done

export OMP_NUM_THREADS=8

for i in {1..20}; do
    ./pi_critical $i 2
done

