#!/bin/sh
#SBATCH --account=icts
#SBATCH --partition=curie
#SBATCH --nodes=1 --ntasks=8
#SBATCH --job-name="Pi_Atomic_Test1"
#SBATCH --time=1:00:00
#SBATCH --mail-user=NDLZAC001@myuct.ac.za
#SBATCH --mail-type=NONE

cd ~/pi-calc/
make pi-atomic

export OMP_NUM_THREADS=2
echo "iteration,total_iterations,numcores,numthreads,time,pi"
for i in {1..20}; do
    ./pi-atomic $i $SLURM_TASKS_PER_NODE
done

export OMP_NUM_THREADS=4
for i in {1..20}; do
    ./pi-atomic $i $SLURM_TASKS_PER_NODE
done

export OMP_NUM_THREADS=8
for i in {1..20}; do
    ./pi-atomic $i $SLURM_TASKS_PER_NODE
done
