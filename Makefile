CC = gcc
BINS = pi-reduce pi-atomic
CFLAGS = -fopenmp -lm

.PHONY: all run-reduce clean

all: $(BINS)

clean:
	rm -f $(BINS)

run-reduce: pi-reduce
	./pi-reduce 1 4

run-atomic: pi-atomic
	./pi-atomic 1 4

%: %.c
	$(CC) $(CFLAGS) -o $@ $<
