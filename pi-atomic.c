#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <omp.h> //include OMP library

int main(int argc, char* argv[]) {
  double niter = 10000000;
  int iteration = atoi(argv[1]);
  int numcores = atoi(argv[2]);
  if (argc > 3) niter = atof(argv[3]);
  double x, y, z, pi;
  int i, numthreads = 0, count = 0;
  double start;
  double end;

  /*
  Start time here
  */
  start = omp_get_wtime();
  // printf("Start time is: %f\n",start);

  #pragma omp parallel private(x, y, z, i) shared(count,numthreads)
    {
    srandom((int)time(NULL) ^ omp_get_thread_num());


    for (i=0; i<niter; ++i) {
      x = (double)random()/RAND_MAX;
      y = (double)random()/RAND_MAX;
      z = sqrt((x*x)+(y*y));
      if (z<=1){
        #pragma omp atomic
          ++count;
      }
    }
    #pragma omp master
      numthreads=omp_get_num_threads();
  }

  end = omp_get_wtime();
  // printf("End time is: %f\n",end);
  /*
  End time here
  */
  pi = ((double)count/(double)(niter*numthreads))*4.0;
  printf("%d,%.0f,%d,%d,%f,%f\n", iteration, niter, numcores, numthreads, end-start, pi);
  return 0;

  }
