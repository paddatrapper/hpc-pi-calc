#include <omp.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

using namespace std;

int main (int argc, char* argv[])
{
	double niter= 10000000;

	int iteration = atoi(argv[1]);
	int numcores = atoi(argv[2]);

	if(argc>3) 
		niter = atof(argv[3]);

	double x, y, z;
	int i;
	int numthreads=1;
	int count=0;

	double start = omp_get_wtime();

		srandom((int)time(NULL));
		for(i=0; i<niter; ++i)
		{
			x = (double)random()/RAND_MAX;
			y = (double)random()/RAND_MAX;
			z = sqrt((x*x)+(y*y));
			if (z<=1)
			{
				++count;
			}

		}

	double end = omp_get_wtime();
	
	double pi = ((double)count/(double)(niter*numthreads))*4.0;
    printf("%d,%.0f,%d,%d,%f,%f\n", iteration, niter, numcores, numthreads, end-start, pi);
}
